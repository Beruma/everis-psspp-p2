package dam2.psspp.p2;

import java.util.Scanner;

/*
 * Realiza un programa Java llamado MostrarCinco que reciba desde línea de comandos un único parámetro y lo muestre por
 * pantalla 5 veces.
 * Utiliza códigos de error para los casos en que el programa finalice de forma incorrecta.
 * A continuación, crea otro programa Java llamado Actividad8, que lea desde el teclado una cadena y se la pase a un
 * nuevo subproceso que cree correspondiente al programa anterior.
 * Este programa(proceso padre) deberá comprobar los códigos de error generados por la salida del proceso hijo y
 * mostrando por pantalla, además de almacenar en el fichero resultados.txt las 5 cadenas en lugar de mostrarlas por
 * pantalla.
 */
public class MostrarCinco {
    public static void main(String[] args) {

        for (int i = 0; i < 5 ; i++) {
            System.out.println("hola");
        }
    }
}
