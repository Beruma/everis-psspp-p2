package dam2.psspp.p2;

import java.io.File;
import java.io.IOException;

public class Actividad8 {
    public static void main(String[] args) {
     Actividad8 lanzador1 = new Actividad8();
     lanzador1.crearProceso("./ficheros/resultado.txt");
    }
    public void crearProceso(String ficheroResultado){
        try{
            ProcessBuilder processBuilder = new ProcessBuilder();
            processBuilder.command("java", "dam2.psspp.p2.MostrarCinco");
            processBuilder.directory(new File("./out/production/ejercicio8tema1/"));
            processBuilder.redirectError(new File("./ficheros/errores.txt"));
            processBuilder.redirectOutput(new File(ficheroResultado));
            processBuilder.start();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
